const express = require("express");
const status = require("http-status");
const { v4: uuidv4 } = require("uuid");
const path = require("path");

const app = express();
const PORT = process.env.PORT || 4000;

app.get("/html", (req, res, next) => {
  let htmlPath = path.join(__dirname, "index.html");
  res.set("Content-Type", "text/html");
  res.status(200);
  res.sendFile(htmlPath, (err) => {
    if (err) res.status(500).send(status[500]).end();
  });
});

app.get("/json", (req, res) => {
  let jsonPath = path.join(__dirname, "jsonData.json");
  res.set("Content-Type", "application/json");
  res.status(200);
  res.sendFile(jsonPath, (err) => {
    if (err) res.status(500).send(status[500]).end();
  });
});

app.get("/uuid", (req, res) => {
  res.status(200);
  res
    .json({
      uuid: uuidv4(),
    })
    .end();
});

app.get("/status/:code", (req, res) => {
  let code = req.params.code;
  console.log();
  if (status[code] != undefined) {
    res.status(code);
    res.send(`Sending response with status ${code}`).end();
  } else {
    res.status(404);
    res.send(`Status ${code} is not valid`).end();
  }
});

app.get("/delay/:delay", (req, res) => {
  let delayInMilleSecond = parseInt(req.params.delay) * 1000;
  setTimeout(() => {
    res.json({
      msg: "success",
    });
  }, delayInMilleSecond);
});

app.use((req, res, next) => {
  //   console.log("not found");
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});
app.use((error, req, res, next) => {
  let code = error.status || 500;
  res.status(code);
  res.send(status[code]).end();
});

app.listen(PORT, () => {
  console.log("Server Started on " + PORT);
});
